cd /home/user/ai_ws
virtualenv --system-site-packages -p python3 ./dnn_venv
source ./dnn_venv/bin/activate
pip install --upgrade pip
pip install --upgrade tensorflow
python -c "import tensorflow as tf; print(tf.__version__)"
pip install keras
python -c 'import keras; print(keras.__version__)'
pip install pydot
pip3 install imgaug
pip install opencv-python
pip install gym

# Things to do with opencv bridge compilation for python 3
# To avoid error in further compilation stating: ImportError: No module named 'em', Error
pip uninstall em
pip install empy

catkin config -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.5m -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.5m.so
# Instruct catkin to install built packages into install place. It is $CATKIN_WORKSPACE/install folder
catkin config --install
# Clone cv_bridge src
git clone https://github.com/ros-perception/vision_opencv.git src/vision_opencv
# Find version of cv_bridge in your repository
apt-cache show ros-kinetic-cv-bridge | grep Version
    Version: 1.12.8-0xenial-20180416-143935-0800
# Checkout right version in git repo. In our case it is 1.12.8
cd src/vision_opencv/
### CHANGE https://stackoverflow.com/questions/49221565/unable-to-use-cv-bridge-with-ros-kinetic-and-python3
### src/vision_opencv/cv_bridge/CMakeLists.txt
### find_package(Boost REQUIRED python3)-->to-->find_package(Boost REQUIRED python-py35)
git checkout 1.12.8
cd ../../
# Build
catkin_make


# Not necessary

cd src
git clone https://github.com/lars76/object-localization.git
cd object-localization
wget http://www.robots.ox.ac.uk/~vgg/data/pets/data/images.tar.gz
wget http://www.robots.ox.ac.uk/~vgg/data/pets/data/annotations.tar.gz
tar xf images.tar.gz
tar xf annotations.tar.gz
mkdir backup
mv images.tar.gz backup
mv annotations.tar.gz backup
mv annotations/xmls/* images/
python3 generate_dataset.py
cd example1
python train_model.py
#Adjust the WEIGHTS_FILE in evaluate_performance.py (given by the last script)
python3 example_1/evaluate_performance.py
python3 example_2/train_model.py
#Adjust the WEIGHTS_FILE in evaluate_performance.py (given by the last script)
python3 example_2/evaluate_performance.py
