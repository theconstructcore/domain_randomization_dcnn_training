#!/usr/bin/env python

import rospy
import actionlib
from play_motion_msgs.msg import PlayMotionAction, PlayMotionGoal
from sensor_msgs.msg import JointState

class MoveTiago(object):

    def __init__(self):

        rospy.loginfo("In Move Tiago Class init...")
        rospy.loginfo("Waiting for play_motion...")
        self.client = actionlib.SimpleActionClient("/play_motion", PlayMotionAction)
        self.client.wait_for_server()
        rospy.loginfo("...connected.")

        rospy.wait_for_message("/joint_states", JointState)
        self.goal = PlayMotionGoal()
        rospy.loginfo("Tiago ready to move!")

    def execute_preloaded_motion(self, motion_name='home', skip_planning=True):
        """
        Executed the motions loaded in the start of the simulation
        From package tiago_bringup , tiago_motions.yaml get all the available motions
        """
        self.goal.motion_name = motion_name
        self.goal.skip_planning = skip_planning

        # We wait for a certain time, depending on the movement because otherwaise the
        # next actions wont be Executed. Time base on the grasp_demo.py.
        if motion_name == "home":
           wait_time = 20.0
           self.goal.skip_planning = False
        elif motion_name == "pregrasp_demo":
            wait_time = 60.0
            self.goal.skip_planning = False
        elif motion_name == "look_at_object_demo":
            wait_time = 10.0
            self.goal.skip_planning = False
        elif motion_name == "look_at_table_demo":
            wait_time = 10.0
            self.goal.skip_planning = True
        else:
            wait_time = 10.0

        rospy.loginfo("Motion "+str(motion_name)+", EXECUTING...")
        self.client.send_goal(self.goal)
        self.client.wait_for_result(rospy.Duration(wait_time))
        rospy.loginfo("Motion "+str(motion_name)+", has been executed!")

    def move_tcp_to_random_pose(self, x_range,y_range,z_range,orientation_XYZW):
        # TODO
        return None

    def move_joints(self, x_range,y_range,z_range,orientation_XYZW):
        # TODO
        return None

    def check_init_joint_pose(self):
        """
        It checks in which pose is so that we dont execute the movement that taes us there
        """
        # TODO: Implement
        return "Nothing"


if __name__ == "__main__":
    rospy.init_node('TiagoMoveTest_node', anonymous=True, log_level=rospy.INFO)
    move_tiago_object = MoveTiago()
    move_tiago_object.execute_preloaded_motion('home')
    move_tiago_object.execute_preloaded_motion('pregrasp')
    move_tiago_object.execute_preloaded_motion('look_at_table_demo')
