#!/usr/bin/env python

import glob
import os
import sys
import xml.etree.ElementTree as ET
print("Start Module Loading...Remove CV ROS-Kinetic version due to incompatibilities")

# We load Python stuff first because afterwards it will be removed to avoid error with openCV
sys.path.append('/usr/lib/python2.7/dist-packages')
import rospy
import rospkg
from geometry_msgs.msg import Pose
from rviz_markers import MarkerBasics

print(sys.path)
try:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
except ValueError:
    print ("Its already removed..../opt/ros/kinetic/lib/python2.7/dist-packages")
print(sys.path)
import cv2
import numpy as np

from train_model import create_model, IMAGE_SIZE, ALPHA
from keras.applications.mobilenetv2 import preprocess_input

import xml.etree.ElementTree as ET

DEBUG = False
WEIGHTS_FILE_NAME = "model-0.01.h5"

def predict_image(path, model):
    im = cv2.imread(path)
    if im.shape[0] != IMAGE_SIZE:
        im = cv2.resize(im, (IMAGE_SIZE, IMAGE_SIZE))

    image = np.array(im, dtype='f')
    image = preprocess_input(image)

    prediction = model.predict(x=np.array([image]))[0]

    return prediction

def show_image(path, wait_time=500):
    image = cv2.imread(path)
    cv2.imshow("image", image)
    print ("Waiting " + str(wait_time) + "ms...")
    cv2.waitKey(wait_time)
    print ("Waiting "+str(wait_time)+"ms...END")
    cv2.destroyAllWindows()

def get_xyz_from_xml(path_xml_file):

    tree = ET.parse(path_xml_file)

    x_com = float(tree.findtext("./object/pose3d/x_com"))
    y_com = float(tree.findtext("./object/pose3d/y_com"))
    z_com = float(tree.findtext("./object/pose3d/z_com"))

    return [x_com, y_com, z_com]

def main():


    rospy.init_node('evaluate_performance_node', anonymous=True, log_level=rospy.WARN)
    rospy.logwarn("Evaluations START")


    # We initialise the Markers for Center Of Mass estimation and the Object real position
    marker_type = "cube"
    namespace = "demo_table"
    mesh_package_path = ""
    demo_table_position = MarkerBasics(type=marker_type,
                                      namespace=namespace,
                                      index=0,
                                      red=0.0,
                                      green=0.0,
                                      blue=1.0,
                                      alfa=1.0,
                                      scale=[0.6,0.6,0.6],
                                      mesh_package_path=mesh_package_path)


    marker_type = "mesh"
    namespace = "spam_real_position"
    mesh_package_path = "dynamic_objects/models/demo_spam/meshes/nontextured_fixed.stl"

    spam_real_position = MarkerBasics(type=marker_type,
                                        namespace=namespace,
                                        index=0,
                                        red=1.0,
                                        green=0.0,
                                        blue=0.0,
                                        alfa=1.0,
                                        scale=[1.0, 1.0, 1.0],
                                        mesh_package_path=mesh_package_path)

    marker_type = "sphere"
    mesh_package_path = ""
    namespace = "com_prediction"
    com_prediction_marker = MarkerBasics(type=marker_type,
                                        namespace=namespace,
                                        index=0,
                                        red=0.0,
                                        green=1.0,
                                        blue=0.0,
                                        alfa=1.0,
                                        scale=[0.1, 0.1, 0.1],
                                        mesh_package_path=mesh_package_path)


    # We start the model in Keras
    model = create_model(IMAGE_SIZE, ALPHA)

    rospack = rospkg.RosPack()
    # get the file path for rospy_tutorials
    path_to_package = rospack.get_path('dcnn_training_pkg')
    models_weight_checkpoints_folder = os.path.join(path_to_package, "model_weight_checkpoints_gen")
    model_file_path = os.path.join(models_weight_checkpoints_folder, WEIGHTS_FILE_NAME)

    model.load_weights(model_file_path)

    testing_unscaled_img_folder = os.path.join(path_to_package, "testing/dataset_gen/images")
    testing_unscaled_anotations_folder = os.path.join(path_to_package, "testing/dataset_gen_annotations")

    print("\nTrying out unscaled image")
    for k in os.listdir(testing_unscaled_img_folder):
        print ("Name File==>" + str(k))
        if "png" in k:
            img_path = os.path.join(testing_unscaled_img_folder, k)
            pred = predict_image(img_path, model)

            base_name_file = os.path.splitext(k)[0]
            annotation_file = base_name_file + ".xml"
            img_anotation_path = os.path.join(testing_unscaled_anotations_folder, annotation_file)
            reality = get_xyz_from_xml(img_anotation_path)

            print ("Class Prediction=>" + str(pred))
            print ("Class Reality=>" + str(reality))

            spam_real_pose = Pose()
            spam_real_pose.position.x = reality[0]
            spam_real_pose.position.y = reality[1]
            spam_real_pose.position.z = reality[2]

            com_prediction_pose = Pose()
            com_prediction_pose.position.x = pred[0]
            com_prediction_pose.position.y = pred[1]
            com_prediction_pose.position.z = pred[2]

            demo_table_pose = Pose()
            demo_table_pose.position.x = 0.0
            demo_table_pose.position.y = 0.0
            demo_table_pose.position.z = 0.3

            spam_real_position.publish_marker(spam_real_pose)
            com_prediction_marker.publish_marker(com_prediction_pose)
            demo_table_position.publish_marker(demo_table_pose)

            show_image(img_path, wait_time=5000)

    print("\nDone")

    rospy.logwarn("Evaluations END")


if __name__ == "__main__":
    main()
