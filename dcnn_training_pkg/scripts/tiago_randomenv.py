#!/usr/bin/env python
import rospy
import time
import random
import gym
import math
import rospkg
import os
import copy
from geometry_msgs.msg import Pose
from randomgazebomanager_pkg.rviz_markers import MarkerBasics
#from randomgazebomanager_pkg.rviz_markers import PickObjectMenu
from sensor_msgs.msg import Image
from get_model_gazebo_pose import GazeboModel
from std_srvs.srv import Empty, EmptyRequest
from tiago_domain_randomization.srv import PickObjectRequest, PickObject
from move_tiago import MoveTiago

# Dont put anything ros related after this import because it removes ROS from imports to
# import the cv2 installed and not the ROS version
from rgb_camera_python3 import RGBCamera
import sys
#print(sys.path)
try:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
except ValueError:
    ImportError
#print(sys.path)
import cv2
import numpy as np

from train_model import create_model
from keras.applications.mobilenetv2 import preprocess_input

import xml.etree.ElementTree as ET


class FetchDCNN():
    def __init__(self, environment_name, weight_file_name, image_size, ALPHA, grasp_activated, number_of_elements_to_be_output, model_to_track_name, table_to_track_name, path_to_dataset_gen, rgb_camera_topic_name):


        self._image_size = image_size
        self._number_of_elements_to_be_output = number_of_elements_to_be_output

        self.init_tiago_pick_service()

        # We Set the robot in the correct position:
        # Init the FetchClient to move the robot arm
        self.move_tiago_object = MoveTiago()

        self.init_rviz_markers()

        # Init service to move fetch, this is because moveit doenst work in python 3
        # TODO: Not used now
        self._grasp_activated = grasp_activated

        # Init camera RGB object
        self.rgb_camera_object = RGBCamera(rgb_camera_topic_name)

        # This are the models that we will generate information about.
        self.model_to_track_name = model_to_track_name
        self.table_to_track_name = table_to_track_name

        model_to_track_list = [self.model_to_track_name, self.table_to_track_name]
        self.gz_model_obj = GazeboModel(model_to_track_list)

        # We start the model in Keras
        self.model = create_model(self._image_size, ALPHA, self._number_of_elements_to_be_output)

        rospack = rospkg.RosPack()
        # get the file path for rospy_tutorials
        models_weight_checkpoints_folder = os.path.join(path_to_dataset_gen, "model_weight_checkpoints_gen")
        model_file_path = os.path.join(models_weight_checkpoints_folder, weight_file_name)

        print (model_file_path)

        self.model.load_weights(model_file_path)

        self.testing_unscaled_img_folder = os.path.join(path_to_dataset_gen, "dataset_gen/images")
        self.testing_unscaled_anotations_folder = os.path.join(path_to_dataset_gen, "dataset_gen_annotations")


        # We reset the environent to a random state
        print("Starting Service to Reset World Randomly....")
        self.dynamic_world_service_call = rospy.ServiceProxy('/dynamic_world_service', Empty)
        self.change_env_request = EmptyRequest()
        self.dynamic_world_service_call(self.change_env_request)
        print("Starting Service to Reset World Randomly....DONE")

    def init_tiago_pick_service(self):
        """
        We initialise the servie that we will use to send the pick object pose
        """
        # We wait for the service
        self.pick_object_service_name = '/pick_object_srvs_server'
        rospy.wait_for_service(self.pick_object_service_name)
        self.pick_object_service = rospy.ServiceProxy(self.pick_object_service_name, PickObject)

    def init_rviz_markers(self):
        """
        We initialise the markers used to visualise the predictions vs the
        real data.
        """
        # We initialise the Markers for Center Of Mass estimation and the Object real position
        marker_type = "cube"
        namespace = "table"
        mesh_package_path = ""
        self.demo_table_position = MarkerBasics(type=marker_type,
                                          namespace=namespace,
                                          index=0,
                                          red=0.0,
                                          green=0.0,
                                          blue=1.0,
                                          alfa=1.0,
                                          scale=[2.0,0.8,1.48],
                                          mesh_package_path=mesh_package_path)


        marker_type = "mesh"
        namespace = "object_real_position"
        mesh_package_path = "dynamic_objects/models/coffee_cup/meshes/coffee_cup_v2.stl"

        self.spam_real_position = MarkerBasics(type=marker_type,
                                            namespace=namespace,
                                            index=0,
                                            red=1.0,
                                            green=0.0,
                                            blue=0.0,
                                            alfa=1.0,
                                            scale=[1.0, 1.0, 1.0],
                                            mesh_package_path=mesh_package_path)

        marker_type = "sphere"
        mesh_package_path = ""
        namespace = "object_prediction"
        self.com_prediction_marker = MarkerBasics(type=marker_type,
                                            namespace=namespace,
                                            index=0,
                                            red=0.0,
                                            green=1.0,
                                            blue=0.0,
                                            alfa=1.0,
                                            scale=[0.1, 0.1, 0.1],
                                            mesh_package_path=mesh_package_path)

        #self.pick_object_menu = PickObjectMenu()


    def publish_markers_new_data(self,pred, reality, reality_table):

        self.publish_reality_markers_new_data(reality)
        self.publish_pred_markers_new_data(pred)
        self.publish_table_markers_new_data(reality_table)

    def publish_reality_markers_new_data(self, reality):

        spam_real_pose = Pose()
        spam_real_pose.position.x = reality[0]
        spam_real_pose.position.y = reality[1]
        spam_real_pose.position.z = reality[2]

        self.spam_real_position.publish_marker(spam_real_pose)

    def publish_pred_markers_new_data(self,pred):

        com_prediction_pose = Pose()
        com_prediction_pose.position.x = pred[0]
        com_prediction_pose.position.y = pred[1]
        com_prediction_pose.position.z = pred[2]

        self.com_prediction_marker.publish_marker(com_prediction_pose)

    def publish_table_markers_new_data(self, reality_table):

        demo_table_pose = Pose()
        demo_table_pose.position.x = reality_table[0]
        demo_table_pose.position.y = reality_table[1]
        demo_table_pose.position.z = reality_table[2]

        self.demo_table_position.publish_marker(demo_table_pose)


    def predict_image(self,model,cv2_img=None, path=None):

        if cv2_img.all() != None:
            im = cv2_img
        else:
            if path != None:
                im = cv2.imread(path)
            else:
                print ("Predict Image had no path image or image CV2")
                return None

        if im.shape[0] != self._image_size:
            im = cv2.resize(im, (self._image_size, self._image_size))
            """
            self.rgb_camera_object.display_image(   image_display=im,
                                                    life_time_ms=50,
                                                    name="ResizedCAM"
                                                )
            """

        image = np.array(im, dtype='f')
        image = preprocess_input(image)


        self.rgb_camera_object.display_image(   image_display=image,
                                                    life_time_ms=50,
                                                    name="ImagePredict"
                                                )

        prediction = model.predict(x=np.array([image]))[0]

        return prediction

    def show_image(self,path, wait_time=500):
        image = cv2.imread(path)
        cv2.imshow("image", image)
        print ("Waiting " + str(wait_time) + "ms...")
        cv2.waitKey(wait_time)
        print ("Waiting "+str(wait_time)+"ms...END")
        cv2.destroyAllWindows()

    def get_xyz_from_xml(self,path_xml_file):

        tree = ET.parse(path_xml_file)

        x_com = float(tree.findtext("./object/pose3d/x_com"))
        y_com = float(tree.findtext("./object/pose3d/y_com"))
        z_com = float(tree.findtext("./object/pose3d/z_com"))

        return [x_com, y_com, z_com]

    def get_xyz_from_world(self, model_name):
        """
        Retrieves the position of an object from the world
        """
        pose_now = self.gz_model_obj.get_model_pose(model_name)

        XYZ = [pose_now.position.x,pose_now.position.y,pose_now.position.z]

        return XYZ


    def start_prediction_test(self):

        print("\nTrying out unscaled image")
        for k in os.listdir(self.testing_unscaled_img_folder):
            print ("Name File==>" + str(k))

            rospy.logwarn("We Reset Simulation")

            init_joints_config = [0.0] * 7
            self.move_joints(init_joints_config)

            if "png" in k:

                img_path = os.path.join(self.testing_unscaled_img_folder, k)
                pred = self.predict_image(img_path, self.model)

                base_name_file = os.path.splitext(k)[0]
                annotation_file = base_name_file + ".xml"
                img_anotation_path = os.path.join(self.testing_unscaled_anotations_folder, annotation_file)
                reality = self.get_xyz_from_xml(img_anotation_path)

                print ("Class Prediction=>" + str(pred))
                print ("Class Reality File Image=>" + str(reality))

                self.publish_markers_new_data(pred, reality)

                self.show_image(img_path, wait_time=5000)

                if self._grasp_activated == True:
                    self.start_grasp_sequence(pred)


        rospy.loginfo("Start Prediction DONE...")

    def start_init_position(self):

        #Init Pose
        # From package tiago_bringup , tiago_motions.yaml get all the available motions
        self.move_tiago_object.execute_preloaded_motion('home')
        self.move_tiago_object.execute_preloaded_motion('pregrasp')
        self.move_tiago_object.execute_preloaded_motion('look_at_table_demo')

    def start_camera_rgb_prediction(self, number_of_tests=1, camera_period = 5.0, wait_reset_period=3.0, go_dustbin=False, z_value= 0.8, acceptable_error=0.01, real_robot=False):

        # In the Real robot we gove the table position, because it could be a bit different
        if real_robot:
            print("Give Table Position for Markers only")
            table_x = float(input("Table X in World Ref="))
            table_y = float(input("Table Y in World Ref="))
            table_z = float(input("Table Z in World Ref="))
            reality_table = [table_x,table_y,table_z]

        #Init Pose
        self.start_init_position()

        for i in range(number_of_tests):


            print ("Number of Image=>" + str(i))

            if not real_robot:
                self.dynamic_world_service_call(self.change_env_request)
                print ("Waiting for Reset Env to settle=>")
                rospy.sleep(wait_reset_period)
                print ("Waiting for Reset Env to settle...DONE")
            else:
                _ = input("Place The Object in a Location...PRESS any key when ready")

            cv2_img = self.rgb_camera_object.get_latest_image()
            pred = self.predict_image(self.model,cv2_img)

            # Add Z Axis value
            pred_mod = [pred[0],pred[1],z_value]

            # If its the real robot we have to give for marking purposes the objects pos to compare
            if not real_robot:
                reality = self.get_xyz_from_world(self.model_to_track_name)
                reality_table = self.get_xyz_from_world(self.table_to_track_name)
            else:
                print("Give Real Position for Coffe_Cup for Markers Only")
                reality_x = float(input("Coffe_Cup X in World Ref="))
                reality_y = float(input("Coffe_Cup Y in World Ref="))
                reality_z = float(input("Coffe_Cup Z in World Ref="))
                reality = [reality_x,reality_y,reality_z]

            print ("Class Prediction=>" + str(pred))
            print ("Class Prediction Corrected=>" + str(pred_mod))
            print ("Class Reality SIMULATION=>" + str(reality))

            # We calculate differences:
            delta_x = pred_mod[0] - reality[0]
            delta_y = pred_mod[1] - reality[1]

            x_within_errormargin = False
            y_within_errormargin = False
            if abs(delta_x) <= acceptable_error:
                print('\x1b[6;30;42m' + 'delta_x='+str(delta_x)+ '\x1b[0m')
                x_within_errormargin = True
            else:
                print('\x1b[0;37;41m' + 'BAD delta_x='+str(delta_x)+ '\x1b[0m')

            if abs(delta_y) <= acceptable_error:
                print('\x1b[6;30;42m' + 'delta_y='+str(delta_x)+ '\x1b[0m')
                y_within_errormargin = True
            else:
                print('\x1b[0;37;41m' + 'BAD delta_y='+str(delta_y)+ '\x1b[0m')

            print ("Class RealityTable SIMULATION=>" + str(reality_table))
            self.publish_markers_new_data(pred_mod, reality, reality_table)

            # We Give Permission to grasp based in erro of detection
            grasp_allowed = x_within_errormargin and y_within_errormargin
            #self.rgb_camera_object.display_latest_image(int(camera_period*1000))


            if self._grasp_activated == True:
                if grasp_allowed:
                    print('\x1b[6;30;42m' + 'Grasp Allowed='+str(delta_x)+ '\x1b[0m')
                    if go_dustbin:
                        self.start_grasp_sequence_leave_dustbin(pred_mod)
                    else:
                        self.start_grasp_sequence(pred_mod)
                else:
                    print('\x1b[0;37;41m' + 'grasp NOT allowed, error margin exceeded='+ '\x1b[0m')

        rospy.loginfo("Start Prediction DONE...")


    def start_camera_rgb_prediction_continuous(self, image_freq= 20.0, real_robot=False, z_value= 0.8):
        """
        It continuously make predictions
        """
        if not real_robot:
            # We reset the world Once
            self.dynamic_world_service_call(self.change_env_request)
            print ("Waiting for Reset Env to settle=>")
            wait_reset_period = 3.0
            rospy.sleep(wait_reset_period)
            print ("Waiting for Reset Env to settle...DONE")
        else:
            _ = input("Place The Object in a Location...PRESS any key when ready")

        #Init Pose
        self.start_init_position()

        # In the Real robot we gove the table position, because it could be a bit different
        if real_robot:
            print("Give Table Position for Markers only")
            table_x = float(input("Table X in World Ref="))
            table_y = float(input("Table Y in World Ref="))
            table_z = float(input("Table Z in World Ref="))
            reality_table = [table_x,table_y,table_z]

        rate = rospy.Rate(image_freq)
        life_time_ms = int((1.0/ image_freq)*1000)
        while not rospy.is_shutdown():

            cv2_img = self.rgb_camera_object.get_latest_image()

            self.rgb_camera_object.display_image(   image_display=cv2_img,
                                                    life_time_ms=life_time_ms
                                                )

            pred = self.predict_image(self.model,cv2_img)
            # Add Z Axis value
            pred_mod = [pred[0],pred[1],z_value]

            # If its the real robot we have to give for marking purposes the objects pos to compare
            if not real_robot:
                reality = self.get_xyz_from_world(self.model_to_track_name)
                reality_table = self.get_xyz_from_world(self.table_to_track_name)
            else:
                print("Give Real Position for Coffe_Cup for Markers Only")
                reality_x = float(input("Coffe_Cup X in World Ref="))
                reality_y = float(input("Coffe_Cup Y in World Ref="))
                reality_z = float(input("Coffe_Cup Z in World Ref="))
                reality = [reality_x,reality_y,reality_z]


            print ("Class Prediction=>" + str(pred))
            print ("Class PredictionMod=>" + str(pred_mod))
            print ("Class Reality SIMULATION=>" + str(reality))
            self.publish_markers_new_data(pred_mod, reality, reality_table)
            rate.sleep()

        rospy.loginfo("Start Prediction DONE...")


    def check_if_object_grasped(self, tcp_xyz, wait_time=2.0, max_delta=0.2):
        """
        It checks if the object to graps has been lifted from the table
        """

        model_to_track_reality = self.get_xyz_from_world(self.model_to_track_name)

        print ("model_to_track_reality=>" + str(model_to_track_reality))
        print ("tcp_xyz=>" + str(tcp_xyz))

        z_model = model_to_track_reality[2]
        z_tcp = tcp_xyz[2]

        delta = z_tcp - z_model

        rospy.logwarn("delta=="+str(delta)+" <= "+str(max_delta))

        if delta <= max_delta:
            grasp_state = "grasp_success"
        else:
            grasp_state = "grasp_fail"

        #self.publish_new_grasp_state(grasp_state)

        rospy.sleep(wait_time)

        return grasp_state == "grasp_success"


    def start_grasp_sequence(self, predicted_position_XYZ):

        pick_object_object = PickObjectRequest()
        pick_object_object.pick_object_pose_stamped.header.frame_id = "world"
        pick_object_object.pick_object_pose_stamped.pose.position.x = predicted_position_XYZ[0]
        pick_object_object.pick_object_pose_stamped.pose.position.y = predicted_position_XYZ[1]
        pick_object_object.pick_object_pose_stamped.pose.position.z = predicted_position_XYZ[2]

        # Now we Move Tiago. For that we have to publish inside a service
        result = self.pick_object_service(pick_object_object)
        rospy.loginfo("Result Msg="+result.result_info.data)

        if "ERROR" in result.result_info.data:
            rospy.logerr("Something went wrong in the Pick Object Action")
        elif "SUCCESS" in result.result_info.data:
            rospy.loginfo("Pick and place action went ok")
        else:
            rospy.logerr("Something went wrong in pick_object_service")

        # We check if we grapsed the object
        # TODO: Create a New Check Object Grasped
        # self.check_if_object_grasped(tcp_xyz=position_XYZ)


    def start_grasp_sequence_leave_dustbin(self, predicted_position_XYZ):
        # TODO: Create a sequence that places the object on Barista
        pass



if __name__ == '__main__':
    rospy.init_node('fetch_randomenv_node', anonymous=True, log_level=rospy.INFO)


    if len(sys.argv) < 19:
        rospy.logfatal("usage: fetch_randomenv.py environment_name weight_file_name grasp_activated number_of_tests camera_period image_size ALPHA number_of_elements_to_be_output go_dustbin rgb_camera_topic_name z_value real_robot acceptable_error continuous_prediction_image_freq")
    else:

        rospy.logwarn(str(sys.argv))

        environment_name = sys.argv[1]
        weight_file_name = sys.argv[2]
        grasp_activated = bool(sys.argv[3] == "True")
        number_of_tests = int(sys.argv[4])
        camera_period = float(sys.argv[5])
        image_size = int(sys.argv[6])
        ALPHA = float(sys.argv[7])
        number_of_elements_to_be_output = int(sys.argv[8])
        go_dustbin = bool(sys.argv[9] == "True")
        model_to_track_name = sys.argv[10]
        table_to_track_name = sys.argv[11]
        path_to_dataset_gen = sys.argv[12]
        rgb_camera_topic_name = sys.argv[13]
        z_value = float(sys.argv[14])
        real_robot = bool(sys.argv[15] == "True")
        acceptable_error = float(sys.argv[16])
        continuous_prediction = bool(sys.argv[17] == "True")
        continuous_prediction_image_freq = float(sys.argv[18])

        rospy.logwarn("environment_name:"+str(environment_name))
        rospy.logwarn("weight_file_name:"+str(weight_file_name))
        rospy.logwarn("grasp_activated:"+str(grasp_activated))
        rospy.logwarn("number_of_tests:"+str(number_of_tests))
        rospy.logwarn("camera_period:"+str(camera_period))
        rospy.logwarn("image_size:"+str(image_size))
        rospy.logwarn("ALPHA:"+str(ALPHA))
        rospy.logwarn("number_of_elements_to_be_output:"+str(number_of_elements_to_be_output))
        rospy.logwarn("model_to_track_name:"+str(model_to_track_name))
        rospy.logwarn("table_to_track_name:"+str(table_to_track_name))
        rospy.logwarn("path_to_dataset_gen:"+str(path_to_dataset_gen))
        rospy.logwarn("rgb_camera_topic_name:"+str(rgb_camera_topic_name))
        rospy.logwarn("z_value:"+str(z_value))
        rospy.logwarn("real_robot:"+str(real_robot))
        rospy.logwarn("acceptable_error:"+str(acceptable_error))
        rospy.logwarn("continuous_prediction:"+str(continuous_prediction))
        rospy.logwarn("continuous_prediction_image_freq:"+str(continuous_prediction_image_freq))



        agent = FetchDCNN(  environment_name,weight_file_name,
                            image_size,
                            ALPHA,
                            grasp_activated,
                            number_of_elements_to_be_output,
                            model_to_track_name,
                            table_to_track_name,
                            path_to_dataset_gen,
                            rgb_camera_topic_name)

        if continuous_prediction:
            agent.start_camera_rgb_prediction_continuous(image_freq= continuous_prediction_image_freq,
                                                         real_robot= real_robot,
                                                         z_value= z_value)
        else:
            agent.start_camera_rgb_prediction(number_of_tests= number_of_tests,
                                                    camera_period = camera_period,
                                                    wait_reset_period=3.0,
                                                    go_dustbin=go_dustbin,
                                                    z_value= z_value,
                                                    acceptable_error= acceptable_error,
                                                    real_robot= real_robot)

